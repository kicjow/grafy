﻿namespace grafy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PanelPaint1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PanelPaint2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CofnijButton1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.CofonijButton2 = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.56779F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.4322F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1151, 551);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(569, 443);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PanelPaint1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(569, 443);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Graf 1";
            // 
            // PanelPaint1
            // 
            this.PanelPaint1.BackColor = System.Drawing.Color.White;
            this.PanelPaint1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPaint1.Location = new System.Drawing.Point(3, 16);
            this.PanelPaint1.Name = "PanelPaint1";
            this.PanelPaint1.Size = new System.Drawing.Size(563, 424);
            this.PanelPaint1.TabIndex = 0;
            this.PanelPaint1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PanelPaint1_MouseDoubleClick);
            this.PanelPaint1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelPaint1_MouseDown);
            this.PanelPaint1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PanelPaint1_MouseUp);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(578, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(570, 443);
            this.panel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PanelPaint2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(570, 443);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Graf 2";
            // 
            // PanelPaint2
            // 
            this.PanelPaint2.BackColor = System.Drawing.Color.White;
            this.PanelPaint2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPaint2.Location = new System.Drawing.Point(3, 16);
            this.PanelPaint2.Name = "PanelPaint2";
            this.PanelPaint2.Size = new System.Drawing.Size(564, 424);
            this.PanelPaint2.TabIndex = 0;
            this.PanelPaint2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PanelPaint2_MouseDoubleClick);
            this.PanelPaint2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelPaint2_MouseDown);
            this.PanelPaint2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PanelPaint2_MouseUp);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.CofnijButton1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 452);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(569, 96);
            this.panel3.TabIndex = 2;
            // 
            // CofnijButton1
            // 
            this.CofnijButton1.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton1.Name = "CofnijButton1";
            this.CofnijButton1.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton1.TabIndex = 1;
            this.CofnijButton1.Text = "Cofnij1";
            this.CofnijButton1.UseVisualStyleBackColor = true;
            this.CofnijButton1.Click += new System.EventHandler(this.CofnijButton1_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.CofonijButton2);
            this.panel4.Controls.Add(this.OKButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(578, 452);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(570, 96);
            this.panel4.TabIndex = 3;
            // 
            // CofonijButton2
            // 
            this.CofonijButton2.Location = new System.Drawing.Point(3, 3);
            this.CofonijButton2.Name = "CofonijButton2";
            this.CofonijButton2.Size = new System.Drawing.Size(75, 23);
            this.CofonijButton2.TabIndex = 2;
            this.CofonijButton2.Text = "Cofnij2";
            this.CofonijButton2.UseVisualStyleBackColor = true;
            this.CofonijButton2.Click += new System.EventHandler(this.CofonijButton2_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(486, 64);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 1;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 551);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel PanelPaint1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel PanelPaint2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CofnijButton1;
        private System.Windows.Forms.Button CofonijButton2;
    }
}

