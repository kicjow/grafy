﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using grafy.viewModels;
using operacje;

namespace grafy
{
    public partial class Form1 : Form
    {
        private Panel _panelSender;
        private MouseEventArgs _mouseEventArgsSender;


        private const int _szerokoscWierzcholka = 50;
        public Form1()
        {
            InitializeComponent();
        }

        private void CofnijButton1_Click(object sender, EventArgs e)
        {
            PanelSerwer.Cofnij(sender as Panel, PanelEnum.Panel1);
        }

        private void CofonijButton2_Click(object sender, EventArgs e)
        {
            PanelSerwer.Cofnij(sender as Panel, PanelEnum.Panel2);
        }


        private void PanelPaint1_MouseUp(object sender, MouseEventArgs e)
        {
            WalnijKrawedz(sender, e, PanelEnum.Panel1);
        }

        private void PanelPaint1_MouseDown(object sender, MouseEventArgs e)
        {
            ZacznijWalenieKrawedzi(sender, e);
        }

        private void PanelPaint1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            PanelSerwer.NarysujWierzcholek(sender as Panel, e, _szerokoscWierzcholka, PanelEnum.Panel1);
        }

        private void PanelPaint2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            PanelSerwer.NarysujWierzcholek(sender as Panel, e, _szerokoscWierzcholka, panelenum: PanelEnum.Panel2);
        }

        private void PanelPaint2_MouseUp(object sender, MouseEventArgs e)
        {
            WalnijKrawedz(sender, e, PanelEnum.Panel2);
        }

        private void PanelPaint2_MouseDown(object sender, MouseEventArgs e)
        {
            ZacznijWalenieKrawedzi(sender, e);
        }

        private void WalnijKrawedz(object sender, MouseEventArgs e, PanelEnum panelEnum)
        {
            if (sender == _panelSender)
            {
                PanelSerwer.WalnijKrawedz(sender as Panel, _mouseEventArgsSender.X, _mouseEventArgsSender.Y, e.X, e.Y,
               panelEnum, _szerokoscWierzcholka);

            }
            _panelSender = null;
            _mouseEventArgsSender = null;
        }

        private void ZacznijWalenieKrawedzi(object sender, MouseEventArgs e)
        {
            _panelSender = sender as Panel;
            _mouseEventArgsSender = e;

        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            var badacz = new BadaczIzomorfizmuGrafow(PanelSerwer._kolejkaPanelu1, PanelSerwer._kolejkaPanelu2);
            badacz.Zbadaj();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


    }
}
