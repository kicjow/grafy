﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using grafy.viewModels;

namespace grafy
{
    public partial class ZapytajOKrawedzForm : Form
    {
        public ZapytajOKrawedzForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void ZapytajOKrawedzForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (DataIsValid())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Zadna krawedz nie jest zaznaczona");
            }

        }

        private bool DataIsValid()
        {
            if (checkBox1.Checked || checkBox2.Checked)
            {
                if (checkBox1.Checked)
                {
                    var krawedzData = new KrawedzDTO() { Waga = (int)numericUpDown1.Value, CzyMaIstniec = true };
                    this.ZWyzszegoDoNizszego = krawedzData;
                }
                if (checkBox2.Checked)
                {
                    var krawedzData = new KrawedzDTO() { Waga = (int)numericUpDown2.Value, CzyMaIstniec = true };
                    this.ZNizszegoDoWyzszego = krawedzData;

                }
                return true;
            }
            return false;
        }

        public KrawedzDTO ZWyzszegoDoNizszego { get; set; }
        public KrawedzDTO ZNizszegoDoWyzszego { get; set; }
    }
}
