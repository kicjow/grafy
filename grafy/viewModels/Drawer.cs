﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using models;

namespace grafy.viewModels
{
    public class Drawer
    {
        private static int _szerokoscWierzcholka;
        public static void RysujWierzcholek(Panel panel, MouseEventArgs e, int szerokoscWierzcholka, Stack<IDrawCommand> kolejka)
        {
            _szerokoscWierzcholka = szerokoscWierzcholka;

            int x, y;
            DrawerValidator.PoprawKoordynatyKlikniecia(e.X, e.Y, panel.Left, panel.Top, panel.Width, panel.Height, out x, out y, _szerokoscWierzcholka); // by nie wychodził poza ekran.

            if (DrawerValidator.ZobaczCzyMoznaTuCosNarysowac(kolejka, e.X, e.Y, szerokoscWierzcholka)) //czy nie próbujesz narysować wierzchołka na wierzchołku
            {
                return;
            }
            var model = new WierzcholekViewModel(panel, x, y, szerokoscWierzcholka, kolejka.Count);
            kolejka.Push(model); //wrzucam na srtos
            model.Rysuj();
        }

        public static void CofnijWierzcholek(Panel panel, Stack<IDrawCommand> kolejka)
        {
            if (kolejka.Count == 0)
            {
                return;
            }
            var model = kolejka.Pop(); // zdejmuje ze stosu
            model.Usun();
        }

        internal static void NarysujKrawedz(WierzcholekViewModel wierzcholekStartu, WierzcholekViewModel wierzcholekKonca, KrawedzDTO krawedzZNizszegoDoWYzszego, KrawedzDTO krawedzZWyzszegoDoNizszego, Stack<IDrawCommand> kolejka, Panel panel)
        {
            if (krawedzZNizszegoDoWYzszego != null && krawedzZNizszegoDoWYzszego.CzyMaIstniec)
            {
                var nizszy = wierzcholekStartu.Kolejnosc > wierzcholekKonca.Kolejnosc
                    ? wierzcholekKonca
                    : wierzcholekStartu;

                var wyzszy = wierzcholekStartu.Kolejnosc < wierzcholekKonca.Kolejnosc
                    ? wierzcholekKonca
                    : wierzcholekStartu;

                var krawedz = new KrawedzViewModel(nizszy, wyzszy, krawedzZNizszegoDoWYzszego.Waga, panel);
                kolejka.Push(krawedz);
                krawedz.Rysuj();
            }
            if (krawedzZWyzszegoDoNizszego != null && krawedzZWyzszegoDoNizszego.CzyMaIstniec)
            {
                var nizszy = wierzcholekStartu.Kolejnosc > wierzcholekKonca.Kolejnosc
                   ? wierzcholekKonca
                   : wierzcholekStartu;

                var wyzszy = wierzcholekStartu.Kolejnosc < wierzcholekKonca.Kolejnosc
                    ? wierzcholekKonca
                    : wierzcholekStartu;

                var krawedz = new KrawedzViewModel(wyzszy, nizszy, krawedzZWyzszegoDoNizszego.Waga, panel);
                kolejka.Push(krawedz);
                krawedz.Rysuj();
            }
        }
    }
}
