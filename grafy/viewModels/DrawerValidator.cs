﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using models;

namespace grafy.viewModels
{
    public class DrawerValidator
    {
        internal static bool CzyNieistniejeTakaParaINieKonczySieNaSobie(WierzcholekViewModel wierzcholekStartu, WierzcholekViewModel wierzcholekKonca, Stack<IDrawCommand> kolejka)
        {
            if (wierzcholekStartu == wierzcholekKonca)
            {
                return false;
            }

            //foreach (IDrawCommand item in kolejka)
            //{
            //    if (item is KrawedzViewModel)
            //    {
            //        var krawedz = item as KrawedzViewModel;
            //        if (krawedz.WierzcholekKoniec == wierzcholekStartu || krawedz.WierzcholekKoniec == wierzcholekKonca && krawedz.WierzcholekKoniec == wierzcholekStartu || krawedz.WierzcholekStart == wierzcholekStartu)
            //        {
            //            return false;
            //        }
            //    }
            //}
            //nie chce mi sie jednak tego walidowac
            return true;
        }

        public static bool ZobaczCzyMoznaTuCosNarysowac(Stack<IDrawCommand> kolejka, int eX, int eY, int szerokoscWierzcholka)
        {
            var czyJest = false;
            foreach (IDrawCommand item in kolejka)
            {
                if (item is WierzcholekViewModel)
                {
                    var wierz = item as WierzcholekViewModel;
                    var czyX = eX > wierz.X - szerokoscWierzcholka + 5 && eX < wierz.X + szerokoscWierzcholka + 5;
                    var czyY = eY > wierz.Y - szerokoscWierzcholka + 5 && eY < wierz.Y + szerokoscWierzcholka + 5;

                    if (czyX && czyY)
                    {
                        czyJest = true;
                        break;
                    }
                }
            }

            return czyJest;
        }

        public static void PoprawKoordynatyKlikniecia(int cX, int cY, int panelTopLeftX, int panelTopLeftY, int panelWidth, int panelHeight, out int ostateczneKoordynatyX, out int ostatecznekoordynatyY, int _szerokoscWierzcholka) // int panelWidth, int panelHeight,
        {
            var odlegloscXLeft = Math.Abs(cX - panelTopLeftX);
            if (odlegloscXLeft <= _szerokoscWierzcholka / 2)
            {
                //zbytLeft = true;
                cX = _szerokoscWierzcholka / 2;
            }
            var odlegloscXRight = Math.Abs(cX - (panelTopLeftX + panelWidth));

            if (odlegloscXRight <= _szerokoscWierzcholka / 2)
            {
                //zbytRight = true;
                cX = (panelWidth) - _szerokoscWierzcholka / 2;
            }

            var odlegloscTopY = Math.Abs(cY - panelTopLeftY);
            if (odlegloscTopY <= _szerokoscWierzcholka / 2)
            {
                //zbyt top
                cY = _szerokoscWierzcholka / 2;
            }

            var odlegloscBot = Math.Abs(cY - (panelTopLeftY + panelHeight));

            if (odlegloscBot <= _szerokoscWierzcholka / 2)
            {
                //zbytBot = true;
                cY = (panelHeight) - _szerokoscWierzcholka / 2;
            }

            ostateczneKoordynatyX = cX;
            ostatecznekoordynatyY = cY;
        }
        /// <summary>
        /// patrzy czy krawedz ktora chcemy poprowadzic nie zaczyna się gdzieś poza wierzchołkiem
        /// </summary>
        /// <param name="XStart"></param>
        /// <param name="YStart"></param>
        /// <param name="wierzcholekStartu"></param>
        /// <param name="kolejka"></param>
        /// <param name="_szerokoscWierzcholka"></param>
        /// <returns></returns>
        internal static bool CzyPunkNaWierzcholku(int XStart, int YStart, out WierzcholekViewModel wierzcholekStartu, Stack<IDrawCommand> kolejka, int _szerokoscWierzcholka)
        {
            wierzcholekStartu = null;
            var czyJest = false;
            foreach (IDrawCommand item in kolejka)
            {
                if (item is WierzcholekViewModel)
                {
                    var wierz = item as WierzcholekViewModel;
                    var czyX = XStart > wierz.X - _szerokoscWierzcholka && XStart < wierz.X + _szerokoscWierzcholka;
                    var czyY = YStart > wierz.Y - _szerokoscWierzcholka && YStart < wierz.Y + _szerokoscWierzcholka;

                    if (czyX && czyY)
                    {
                        wierzcholekStartu = wierz;
                        czyJest = true;
                        break;
                    }
                }
            }

            return czyJest;
        }
    }
}
