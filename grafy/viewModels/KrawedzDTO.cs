﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grafy.viewModels
{
    public class KrawedzDTO
    {
        public bool CzyMaIstniec { get; set; }
        public int Waga { get; set; }
    }
}
