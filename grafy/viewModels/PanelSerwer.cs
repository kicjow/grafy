﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using models;

namespace grafy.viewModels
{
    public static class PanelSerwer
    {
        public static readonly Stack<IDrawCommand> _kolejkaPanelu1;

        private static Drawer _drawer;
        public static readonly Stack<IDrawCommand> _kolejkaPanelu2;


        static PanelSerwer()
        {
            _kolejkaPanelu1 = new Stack<IDrawCommand>();
            _kolejkaPanelu2 = new Stack<IDrawCommand>();
        }

        internal static void NarysujWierzcholek(Panel panel, MouseEventArgs e, int szerokoscWierzcholka, PanelEnum panelenum)
        {

            Stack<IDrawCommand> kolejka;
            kolejka = panelenum == PanelEnum.Panel1 ? _kolejkaPanelu1 : _kolejkaPanelu2;

            Drawer.RysujWierzcholek(panel, e, szerokoscWierzcholka, kolejka);
        }

        internal static void Cofnij(Panel panel, PanelEnum panelEnum)
        {
            Stack<IDrawCommand> kolejka;
            kolejka = panelEnum == PanelEnum.Panel1 ? _kolejkaPanelu1 : _kolejkaPanelu2;
            Drawer.CofnijWierzcholek(panel, kolejka);
        }

        internal static void WalnijKrawedz(Panel panel, int XStart, int YStart, int XKoniec, int YKoniec, PanelEnum panelEnum, int _szerokoscWierzcholka)
        {
            WierzcholekViewModel wierzcholekStartu, wierzcholekKonca;
            Stack<IDrawCommand> kolejka;
            kolejka = panelEnum == PanelEnum.Panel1 ? _kolejkaPanelu1 : _kolejkaPanelu2;

            bool czyDobryStart = DrawerValidator.CzyPunkNaWierzcholku(XStart, YStart, out wierzcholekStartu, kolejka, _szerokoscWierzcholka);
            bool czyDobryKoniec = DrawerValidator.CzyPunkNaWierzcholku(XKoniec, YKoniec, out wierzcholekKonca, kolejka, _szerokoscWierzcholka);
            bool czyDobrePolaczenie = DrawerValidator.CzyNieistniejeTakaParaINieKonczySieNaSobie(wierzcholekStartu, wierzcholekKonca, kolejka);

            if (CzyMamyDaneByNarysowacKrawedz(czyDobryStart, czyDobryKoniec, czyDobrePolaczenie, wierzcholekStartu, wierzcholekKonca))
            {
                KrawedzDTO krawedzZNizszegoDoWYzszego = null, krawedzZWyzszegoDoNizszego = null;
                //  TypKrawedziEnum skierowanieKrawedziEnum;
                bool czyDalInfo = PoprosOWageIKierunekProstej(ref krawedzZNizszegoDoWYzszego, ref krawedzZWyzszegoDoNizszego);
                if (czyDalInfo)
                {
                    Drawer.NarysujKrawedz(wierzcholekStartu, wierzcholekKonca, krawedzZNizszegoDoWYzszego,
                        krawedzZWyzszegoDoNizszego, kolejka, panel);
                }

            }

        }

        private static bool PoprosOWageIKierunekProstej(ref KrawedzDTO wagaZNiszegoDoWyzszego, ref KrawedzDTO WagaZWyzszegoDoNizszego)
        {
            bool czyJestOK = false;
            using (var forma = new ZapytajOKrawedzForm())
            {
                forma.ShowDialog();
                if (forma.DialogResult == DialogResult.OK)
                {
                    if (forma.ZNizszegoDoWyzszego != null && forma.ZNizszegoDoWyzszego.CzyMaIstniec)
                    {
                        wagaZNiszegoDoWyzszego = forma.ZNizszegoDoWyzszego;
                        czyJestOK = true;
                    }
                    if (forma.ZWyzszegoDoNizszego != null && forma.ZWyzszegoDoNizszego.CzyMaIstniec)
                    {
                        WagaZWyzszegoDoNizszego = forma.ZWyzszegoDoNizszego;
                        czyJestOK = true;
                    }
                }
            }
            return czyJestOK;
        }

        private static bool CzyMamyDaneByNarysowacKrawedz(bool czyDobryStart, bool czyDobryKoniec, bool czyDobrePolaczenie, WierzcholekViewModel wierzcholekStartu, WierzcholekViewModel wierzcholekKonca)
        {
            return czyDobryStart && czyDobryKoniec && czyDobrePolaczenie && wierzcholekStartu != null & wierzcholekKonca != null;
        }




    }
}
