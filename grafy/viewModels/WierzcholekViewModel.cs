﻿using System.Drawing;
using System.Globalization;
using models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace grafy.viewModels
{
    public class WierzcholekViewModel : IDrawCommand, IShape
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int SzerokoscWierzcholka { get; private set; }
        private Panel _panel;
        public int Kolejnosc;
        public WierzcholekViewModel(Panel panel, int x, int y, int szerokoscWierzcholka, int kolejnosc)
        {
            X = x;
            Y = y;
            SzerokoscWierzcholka = szerokoscWierzcholka;
            _panel = panel;
            Kolejnosc = kolejnosc;
            Wierzcholek = new Wierzcholek();
            Wierzcholek.Id = kolejnosc;
        }
        public Wierzcholek Wierzcholek { get; set; }

        public void Usun()
        {
            var color = new SolidBrush(Color.White);
            Graphics g = _panel.CreateGraphics();
            var pen = new Pen(color);
            pen.Width = 5;
            g.DrawString(Kolejnosc.ToString(CultureInfo.InvariantCulture), new Font("Arial", 12), color, X - SzerokoscWierzcholka / 5, Y - SzerokoscWierzcholka / 5);
            g.DrawEllipse(pen, X - SzerokoscWierzcholka / 2, Y - SzerokoscWierzcholka / 2, SzerokoscWierzcholka, SzerokoscWierzcholka);
            g.FillEllipse(color, X - SzerokoscWierzcholka / 2, Y - SzerokoscWierzcholka / 2, SzerokoscWierzcholka, SzerokoscWierzcholka);
        }

        public void Rysuj()
        {

            var color = new SolidBrush(Color.Black);
            var pen = new Pen(color);
            pen.Width = 5;
            Graphics g = _panel.CreateGraphics();
            g.DrawEllipse(pen, X - SzerokoscWierzcholka / 2, Y - SzerokoscWierzcholka / 2, SzerokoscWierzcholka,
                SzerokoscWierzcholka);
            g.DrawString(Kolejnosc.ToString(CultureInfo.InvariantCulture), new Font("Arial", 12), color,
                X - SzerokoscWierzcholka / 5, Y - SzerokoscWierzcholka / 5);
        }

    }
}
