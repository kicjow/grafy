﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace models
{
    public class Krawedz
    {
        public Wierzcholek WierzcholekA { get; set; }
        public Wierzcholek WierzcholekB { get; set; }
        public int Waga { get; set; }

    }
}
