﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using models;

namespace models
{
    public class KrawedzViewModel : IDrawCommand
    {
        public WierzcholekViewModel WierzcholekStart;
        public WierzcholekViewModel WierzcholekKoniec;
        public int Waga;
        public Panel Panel;
        public Krawedz Krawedz { get; set; }

        public KrawedzViewModel(WierzcholekViewModel start, WierzcholekViewModel koniec, int waga, Panel panel)
        {
            WierzcholekStart = start;
            WierzcholekKoniec = koniec;
            Waga = waga;
            this.Panel = panel;
            Krawedz = new Krawedz();
            Krawedz.WierzcholekA = WierzcholekStart.Wierzcholek;
            Krawedz.WierzcholekB = WierzcholekKoniec.Wierzcholek;
            Krawedz.Waga = waga;
        }



        public void Rysuj()
        {
            var color = new SolidBrush(Color.Black);
            var pen = new Pen(color);
            pen.Width = 5;
            var g = Panel.CreateGraphics();
            g.DrawLine(pen, new Point(WierzcholekKoniec.X, WierzcholekKoniec.Y), new Point(WierzcholekStart.X, WierzcholekStart.Y));
            int x, y;
            ZnajdzLinieWpolowie(WierzcholekStart.X, WierzcholekStart.Y, WierzcholekKoniec.X, WierzcholekKoniec.Y, out x, out y);
            g.DrawString(Waga.ToString(), new Font("Arial", 8), new SolidBrush(Color.Red), x, y);
        }

        public void Usun()
        {
            var color = new SolidBrush(Color.White);
            var pen = new Pen(color);
            pen.Width = 5;
            var g = Panel.CreateGraphics();
            g.DrawLine(pen, new Point(WierzcholekKoniec.X, WierzcholekKoniec.Y), new Point(WierzcholekStart.X, WierzcholekStart.Y));
            int x, y;
            ZnajdzLinieWpolowie(WierzcholekStart.X, WierzcholekStart.Y, WierzcholekKoniec.X, WierzcholekKoniec.Y, out x, out y);
            g.DrawString(Waga.ToString(), new Font("Arial", 10), new SolidBrush(Color.Red), x, y);
        }

        private void ZnajdzLinieWpolowie(int xstart, int ystart, int xkoniec, int ykoniec, out int x, out  int y)
        {
            var wiekszeX = xstart > xkoniec ? xstart : xkoniec;
            var wiekszeY = ystart > ykoniec ? ystart : ykoniec;
            var mniejszeX = xstart < xkoniec ? xstart : xkoniec;
            var mniejszeY = ystart < ykoniec ? ystart : ykoniec;

            x = mniejszeX + (wiekszeX - mniejszeX) / 2;
            y = mniejszeY + (wiekszeY - mniejszeY) / 2;
        }
    }
}
