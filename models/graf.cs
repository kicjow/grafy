﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace models
{
    public class Graf
    {
        public List<Wierzcholek> Wierzcholki { get; set; }
        public List<Krawedz> Krawedzie { get; set; }
    }
}
