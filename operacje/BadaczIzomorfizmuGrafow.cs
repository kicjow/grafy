﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using models;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;

namespace operacje
{
    public class BadaczIzomorfizmuGrafow
    {
        private Stack<IDrawCommand> stack1; //to jest reprezentujący krawędzie i wierzchołki grafu 1
        private Stack<IDrawCommand> stack2;//to jest reprezentujący krawędzie i wierzchołki grafu 2
        private List<Wierzcholek> wierzcholkiGrafu1;
        private List<Krawedz> krawedzieGrafu1;
        private List<Wierzcholek> wierzcholkiGrafu2;
        private List<Krawedz> krawedzieGrafu2;
        private Boolean czyIzometryczne = false;

        public BadaczIzomorfizmuGrafow(Stack<IDrawCommand> stack1, Stack<IDrawCommand> stack2)
        {

            this.stack1 = stack1;
            this.stack2 = stack2;
            wierzcholkiGrafu1 = new List<Wierzcholek>();
            krawedzieGrafu1 = new List<Krawedz>();

            wierzcholkiGrafu2 = new List<Wierzcholek>();
            krawedzieGrafu2 = new List<Krawedz>();

            PrzyporzadkujKrawdzieIGrafyDoList();
        }

        private void PrzyporzadkujKrawdzieIGrafyDoList()
        {
            foreach (var drawCommand in stack1)
            {
                if (drawCommand is KrawedzViewModel)
                {
                    var krawedzVw = drawCommand as KrawedzViewModel;
                    krawedzieGrafu1.Add(krawedzVw.Krawedz);
                }
                if (drawCommand is WierzcholekViewModel)
                {
                    var wierzcholekVW = drawCommand as WierzcholekViewModel;
                    wierzcholkiGrafu1.Add(wierzcholekVW.Wierzcholek);
                }
            }

            foreach (var drawCommand in stack2)
            {
                if (drawCommand is KrawedzViewModel)
                {
                    var krawedzVw = drawCommand as KrawedzViewModel;
                    krawedzieGrafu2.Add(krawedzVw.Krawedz);
                }
                if (drawCommand is WierzcholekViewModel)
                {
                    var wierzcholekVW = drawCommand as WierzcholekViewModel;
                    wierzcholkiGrafu2.Add(wierzcholekVW.Wierzcholek);
                }
            }


        }

        public void Zbadaj()
        {
            //TODO pozostaje ci juz tylko, bazując na listach wierzcholkiGrafu1 krawedzieGrafu1 wierzcholkiGrafu2 krawedzieGrafu2 zrobic macierze sąsiedztwa i zbadać ich równość co do permutacji
            //Grafy planarne są izomorficzne gdy ich macierze sąsiedztwa są równe co do permutacji. Dla grafów skierowanych w macierzach sąsiedztwa bedą cyfry oznaczające koszt drogi. 
            //W macierzy mam taką formułke

            //    a    b    c    d    e
            //a   0    2    1    0    0
            //b   1    0    2    3    0
            //co znacza, że z a można przejsc do b za 2 i do c z 1. etc
            //teraz musze spermutować tą macierz względem kolumn i wierszy, by otrzymać macierz drugiego grafu. I tyle w sumie.

            // sprawdzam czy rozmiary są różne, jeśli tak to izometria odpada
            if (wierzcholkiGrafu1.Count != wierzcholkiGrafu2.Count)
            {
                System.Windows.Forms.MessageBox.Show("Nie ma izometrii");
            }

            int[,] macierz1 = new int[wierzcholkiGrafu1.Count, wierzcholkiGrafu1.Count];
            int[,] macierz2 = new int[wierzcholkiGrafu2.Count, wierzcholkiGrafu2.Count];


            // inicjalizacja zerami
            for (var i = 0; i < macierz1.GetLength(0); i++)
            {
                for (int j = 0; j < macierz1.GetLength(0); j++)
                {
                    macierz1[i, j] = 0;
                }
            }
            for (var i = 0; i < macierz2.GetLength(0); i++)
            {
                for (int j = 0; j < macierz2.GetLength(0); j++)
                {
                    macierz2[i, j] = 0;
                }
            }

            // macierze sąsiedztwa, waga tam gdzie jest krawędź
            foreach (var krawedz in this.krawedzieGrafu1)
            {
                macierz1[krawedz.WierzcholekA.Id, krawedz.WierzcholekB.Id] = krawedz.Waga;
                macierz1[krawedz.WierzcholekB.Id, krawedz.WierzcholekA.Id] = krawedz.Waga;
            }

            foreach (var krawedz in this.krawedzieGrafu2)
            {
                macierz2[krawedz.WierzcholekA.Id, krawedz.WierzcholekB.Id] = krawedz.Waga;
                macierz2[krawedz.WierzcholekB.Id, krawedz.WierzcholekA.Id] = krawedz.Waga;
            }

            // porównuję permutacje macierzy sąsiedztwa. zmieniam macierz1; macierz2 pozostaje niezmienna

            // tworzę array permutacji używany w 2 pętlach - zewnętrznej wierszy i wewnętrznej kolumn
            // dla każdej permutacji (n! * n!) sprawdzam macierz1==macierz2

            IEnumerable<IEnumerable<int>> kolejnoscPermutacji = GetPermutations(Enumerable.Range(1, macierz1.GetLength(0)), macierz1.GetLength(0));

            foreach (var permutacja in kolejnoscPermutacji)
            {
                int[,] zmienioneWiersze = zmienKolejnoscWierszy(macierz1, permutacja);

                //System.Windows.Forms.MessageBox.Show(Helpers.ObjectToString(macierz2));


                if (czyMacierzeTakieSame(zmienioneWiersze, macierz2) && RownaIloscWierzcholkow() && RownaSumaWagaKrawedzi() && RownaIloscKrawedzi())
                {
                    this.czyIzometryczne = true;
                    System.Windows.Forms.MessageBox.Show("Grafy izometryczne");
                    return;
                }
                else
                {
                    foreach (var permutacja2 in kolejnoscPermutacji)
                    {
                        int[,] zmienioneKolumny = zmienKolejnoscKolumn(zmienioneWiersze, permutacja2);
                        if (czyMacierzeTakieSame(zmienioneKolumny, macierz2))
                        {
                            this.czyIzometryczne = true;
                            System.Windows.Forms.MessageBox.Show("Grafy izometryczne");
                            return;
                        }
                    }
                }
            }
            if (!this.czyIzometryczne) System.Windows.Forms.MessageBox.Show("Nie ma izometrii");
        }

        private bool RownaIloscKrawedzi()
        {
            return krawedzieGrafu1.Count == krawedzieGrafu2.Count;
        }

        private bool RownaSumaWagaKrawedzi()
        {
            return krawedzieGrafu1.Sum(x => x.Waga) == krawedzieGrafu2.Sum(x => x.Waga);
        }

        private bool RownaIloscWierzcholkow()
        {
            return wierzcholkiGrafu1.Count == wierzcholkiGrafu2.Count;
        }

        // zmiana kolejności wierszy macierzy int[,] względem permutacji ienumerable
        static int[,] zmienKolejnoscWierszy(int[,] macierz, IEnumerable<int> permutacja)
        {
            int[,] macierzZwroc = new int[macierz.GetLength(0), macierz.GetLength(0)];
            int wiersz = 0;
            foreach (var indeks in permutacja)
            {
                for (int kol = 0; kol < macierz.GetLength(0); kol++)
                {
                    macierzZwroc[wiersz, kol] = macierz[indeks - 1, kol];
                }
                wiersz++;
            }
            return macierzZwroc;
        }

        // zmiana kolejności kolumn macierzy int[,] względem permutacji ienumerable
        static int[,] zmienKolejnoscKolumn(int[,] macierz, IEnumerable<int> permutacja)
        {
            int[,] macierzZwroc = new int[macierz.GetLength(0), macierz.GetLength(0)];
            int kolumna = 0;
            foreach (var indeks in permutacja)
            {
                for (int wier = 0; wier < macierz.GetLength(0); wier++)
                {
                    macierzZwroc[wier, kolumna] = macierz[wier, indeks - 1];
                }
                kolumna++;
            }
            return macierzZwroc;
        }

        //http://stackoverflow.com/questions/756055/listing-all-permutations-of-a-string-integer
        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

        // porównywanie macierzy dwuwymiarowych
        static Boolean czyMacierzeTakieSame(int[,] macierz1, int[,] macierz2)
        {
            if (macierz1.Length != macierz2.Length) return false;

            for (int i = 0; i < macierz1.GetLength(0); i++)
            {
                for (int j = 0; j < macierz1.GetLength(0); j++)
                {
                    if (macierz1[i, j] != macierz2[i, j]) return false;
                }
            }
            return true;
        }


    }

    public static class Helpers
    {
        public static string ObjectToString(Array ar)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                SoapFormatter formatter = new SoapFormatter();
                formatter.Serialize(ms, ar);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public static object ObjectFromString(string s)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(s)))
            {
                SoapFormatter formatter = new SoapFormatter();
                return formatter.Deserialize(ms) as Array;
            }
        }

        public static T ObjectFromString<T>(string s)
        {
            return (T)Helpers.ObjectFromString(s);
        }
    }
}
